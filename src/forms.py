import re
from wtforms import ValidationError
from wtforms.fields import IntegerField
from wtforms.validators import Optional, Length
from wtforms.widgets import HiddenInput
from wtforms_appengine.ndb import model_form
from models import Phantom, Ghost, Whitelist
from master_keys import ghost_master_key
from master_keys import whitelist_master_key

PhantomFormBase = model_form(Phantom,
                             exclude=('ghost', 'created', 'user'),
                             field_args={
                                 'first_name': {
                                     'validators': [Length(max=100)],
                                 },
                                 'last_name': {
                                     'validators': [Length(max=100)],
                                 }
                             })


class PhantomForm(PhantomFormBase):
    """Phantom Form. Inherits PhantomFormBase"""
    id = IntegerField(widget=HiddenInput(), validators=[Optional()])

    def __init__(self, formdata=None, obj=None, prefix='', data=None,
                 meta=None, **kwargs):
        super(PhantomForm, self).__init__(formdata, obj, prefix, data, meta,
                                          **kwargs)

        if obj and obj.key.kind() == Phantom.__name__ and obj.key.integer_id():
            self.id.data = obj.key.integer_id()


GhostFormBase = model_form(Ghost,
                           exclude=('creator',),
                           field_args={
                                 'name': {
                                     'validators': [Length(max=100)],
                                 },
                                 'description': {
                                     'validators': [Length(max=255)],
                                 }
                             })


class GhostForm(GhostFormBase):
    """GhostForm, inherits GhostFormBase"""
    id = IntegerField(widget=HiddenInput(), validators=[Optional()])

    def __init__(self, formdata=None, obj=None, prefix='', data=None,
                 meta=None, **kwargs):
        super(GhostForm, self).__init__(formdata, obj, prefix, data, meta,
                                        **kwargs)

        if obj and obj.key.kind() == Ghost.__name__ and obj.key.integer_id():
            self.id.data = obj.key.integer_id()

    def validate_name(form, field):
        """Validating name and checking if it is unique"""

        if field.data:
            ghost_with_same_name = Ghost.query(ancestor=ghost_master_key()). \
                filter(Ghost.name_lower == field.data.lower()).get()
            if ghost_with_same_name:
                # lower case the value
                field.data = field.data.lower()

                # check if we have an instance and if s
                if not form.id.data or not form.id.data == \
                    ghost_with_same_name.key.integer_id():
                    raise ValidationError(
                        'Ghost with this name already exists. '
                        'Please choose another one!')


WhitelistBase = model_form(Whitelist,
                           field_args={
                                 'email': {
                                     'validators': [Length(max=255)],
                                 }
                             })


class WhiteListForm(WhitelistBase):
    """WhitelistForm"""
    id = IntegerField(widget=HiddenInput(), validators=[Optional()])

    def __init__(self, formdata=None, obj=None, prefix='', data=None,
                 meta=None, **kwargs):

        super(WhiteListForm, self).__init__(formdata, obj, prefix, data, meta,
                                            **kwargs)

        if obj and obj.key.kind() == Whitelist.__name__ and \
            obj.key.integer_id():
            self.id.data = obj.key.integer_id()

    def validate_email(form, field):
        """Validating if the email is unique and valid"""

        if field.data:
            # check if email is valid
            if not re.match(r"[^@]+@[^@]+\.[^@]+", field.data):
                raise ValidationError('Invalid email format!')

            whitelist_obj_same = Whitelist.query(
                ancestor=whitelist_master_key()).filter(
                    Whitelist.email == field.data.lower()).get()
            if whitelist_obj_same:
                # lower case the value
                field.data = field.data.lower()

                # check if we have an instance and if s
                if not form.id.data or not form.id.data == \
                    whitelist_obj_same.key.integer_id():
                    raise ValidationError(
                        'Email with this name already exists. '
                        'Please choose another one!')
