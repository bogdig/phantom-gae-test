from google.appengine.ext import ndb

GHOST_TEAM_NAME = 'default_ghost_team_key'
PHANTOM_NAME_DEFAULT = 'default_phantom_name_key'
WHITELIST_NAME_DEFAULT = 'default_whitelist_name_key'

def ghost_master_key(ghost_master_name=GHOST_TEAM_NAME):
  """Constructs a Datastore key for a GhostTeam entity.
  We use ghost_master_name as the key.
  """
  return ndb.Key('GhostTeam', ghost_master_name)

def whitelist_master_key(whitelist_master_name=WHITELIST_NAME_DEFAULT):
  """Constructs a Datastore key for a WhiteListTeam entity.
  We use whitelist_master_name as the key.
  """
  return ndb.Key('WhiteListTeam', whitelist_master_name)

def phantom_name_master_key(phantom_master_name=PHANTOM_NAME_DEFAULT):
  """Constructs a Datastore key for a PhantomNames entity.
  We use phantom_master_name as the key.
  """
  return ndb.Key('PhantomNames', phantom_master_name)
