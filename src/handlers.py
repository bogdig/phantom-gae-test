# Copyright 2014 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#     Unless required by applicable law or agreed to in writing, software
#     distributed under the License is distributed on an "AS IS" BASIS,
#     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#     See the License for the specific language governing permissions and
#     limitations under the License.
import csv
import random

from google.appengine.api import users
from oauth2client.contrib.appengine import OAuth2Decorator
from base import handlers
from apiclient.discovery import build
from models import Whitelist
from models import Ghost
from models import UserData
from models import Phantom
from master_keys import whitelist_master_key
from master_keys import ghost_master_key
from master_keys import phantom_name_master_key
from forms import PhantomForm
from forms import GhostForm
from forms import WhiteListForm


def whitelisted_required(handler_method):
    """A decorator requiring a user be logged in to access a handler."""

    def check_user(self, *args, **kwargs):
        """
        Method that checks if a user is an admin or user's email is whitelisted
        """
        user = users.get_current_user()
        is_admin = users.is_current_user_admin()

        if not is_admin and not Whitelist.query(
                ancestor=whitelist_master_key()).filter(
                    Whitelist.email == user.email()).count():
            return self.redirect('/')
        return handler_method(self, *args, **kwargs)

    return check_user


decorator = OAuth2Decorator(
    callback_path='/oauth2callback',
    client_id='325083044995-pnmrqn57fi2ib0su9qm5fgp8tqj6v38n.apps.'
              'googleusercontent.com',
    client_secret='1dU7ixIIiUsNokirAOFeAnpK',
    scope='https://www.googleapis.com/auth/plus.me')
service = build('plus', 'v1')


class UserInfoMixin(object):
    """Mixin for providing a method that returns the userdata"""

    def get_user_data(self):
        """
        Method returning the userdata for the current user; If the user data
        doesn't exist, it will be created
        """
        user = users.get_current_user()
        if user:
            userdata = UserData.query(UserData.user_id == user.user_id()).get()
            if not userdata:
                userdata = UserData(user_id=user.user_id(),
                                    email=user.email())
                userdata.put()
            return userdata
        return None


class RootHandler(handlers.BaseHandler):
    """Root Handler that redirects if the user is logged in, otherwise"""

    def get(self):
        """Get for the Root Handler"""
        user = users.get_current_user()
        if user:
            self.redirect('/overview/')
        else:
            self.render('index.html')


class OverviewHandler(handlers.AuthenticatedHandler):
    """Overview Page Handler"""

    @decorator.oauth_required
    def get(self):
        """
        Method that processes GET requests. It renders to the template a list
        of Ghosts as well as the Phantom name, in case the user created one
        """
        me = service.people().get(userId='me').execute(http=decorator.http())
        first_name = me['name'].get('givenName')
        last_name = me['name'].get('familyName')
        my_id = me.get('id')

        user = users.get_current_user()
        ghosts = Ghost.query(ancestor=ghost_master_key()).fetch()

        userdata = UserData.query(UserData.user_id == user.user_id()).get()
        if not userdata:
            userdata = UserData(user_id=user.user_id(), email=user.email())
            userdata.put()

        # check if the current user has a Phantom Name
        user_phantom = Phantom.query(
            ancestor=phantom_name_master_key()).filter(
                Phantom.user.user_id == userdata.user_id).get()

        ghosts_available = False
        ghost_list = Ghost.query(ancestor=ghost_master_key()).fetch()
        for ghost in ghost_list:
            if not ghost.is_reserved:
                ghosts_available = True
                break

        variables = {
            # 'me_details' : me,
            'first_name': first_name,
            'last_name': last_name,
            'id': my_id,
            'email': user.email(),
            'ghosts': ghosts,
            'userdata': userdata,
            'user_is_admin': users.is_current_user_admin(),
            'user_phantom': user_phantom,
            'ghosts_available': ghosts_available
        }
        return self.render('overview.html', variables)

    def DenyAccess(self):
        """Redirects the user to log in in case access is denied"""
        self.redirect(users.create_login_url(self.request.uri))

    def XsrfFail(self):
        """Renders an errors page in case Xsrf failed"""
        self.render('errors.html', {'error': 'XSRF FAILED'})


class AddPhantomName(handlers.AuthenticatedHandler, UserInfoMixin):
    """Handler that handles the first step in Adding a Phantom Name"""

    @decorator.oauth_required
    def get(self):
        """
        Returns an empty or pre-populated form, depending if the user has added
        first name and last name details in the past
        """
        userdata = self.get_user_data()
        phantom = Phantom.query(ancestor=phantom_name_master_key()).filter(
            Phantom.user.user_id == userdata.user_id).get()
        context = {'userdata': userdata,
                   'user_is_admin': users.is_current_user_admin()}
        if phantom:
            form = PhantomForm(obj=phantom)
            form.id.data = phantom.key.id()
        else:
            form = PhantomForm()
        context.update({'form': form})
        return self.render('manage_phantom_name_first.html', context)

    @decorator.oauth_required
    def post(self):
        """
        Provides a form that handles the POST of the first name and last name
        for the Phantom Name; Edits or Adds a new Phantom Name instance
        """
        form = PhantomForm(self.request.POST)
        userdata = self.get_user_data()

        if form.validate():
            # check if the id of the Phantom belongs to the current user
            phantom_id = form.id.data

            # if phantom id exists, means we are editing the Phantom
            if phantom_id:
                phantom_instance = Phantom.get_by_id(
                    int(phantom_id), parent=phantom_name_master_key())

                if phantom_instance.user.user_id == userdata.user_id:
                    form.populate_obj(phantom_instance)
                    phantom_instance.ghost = None
                    phantom_instance.put()
                    return self.redirect('/add-ghost-to-phantom/')
                else:
                    # not allowed, redirect to adding a ghost
                    return self.redirect('/add-phantom/')

            # means we are adding a new phantom here
            else:
                new_phantom = Phantom(user=userdata,
                                      parent=phantom_name_master_key())
                form.populate_obj(new_phantom)
                print 'saving new phantom..'
                new_phantom.put()

                return self.redirect('/add-ghost-to-phantom/')
        else:
            return self.render('manage_phantom_name_first.html',
                               {
                                   'userdata': userdata,
                                   'user_is_admin':
                                       users.is_current_user_admin(),
                                   'form': form
                               })

    def DenyAccess(self):
        """Redirects the user to log in in case access is denied"""
        self.redirect(users.create_login_url(self.request.uri))

    def XsrfFail(self):
        """Renders an errors page in case Xsrf failed"""
        self.render('errors.html', {'error': 'XSRF FAILED'})


class AddGhostToPhantom(handlers.AuthenticatedHandler, UserInfoMixin):
    """
    Handler that manages the second step of adding a ghost the Phantom object
    linked to userdata
    """

    def get_random_free_ghosts(self, nr_ghosts):
        """Method that returns a number of random ghost instances, if they
        exist"""
        ghosts_keys = []
        # get a list of Phantom obj that have ghosts attached
        phantom_list = Phantom.query(
            ancestor=phantom_name_master_key()).filter(
                Phantom.ghost != None).fetch()

        # in case there are phantoms with ghosts, retrieve those ghosts keys
        if phantom_list:
            for phant in phantom_list:
                ghosts_keys.append(phant.ghost)

        # retrieve a list of all Ghosts and then remove the ghosts from
        # ghosts_keys
        ghost_query = Ghost.query(ancestor=ghost_master_key())
        ghosts_to_choose = [gh for gh in ghost_query.fetch()
                            if gh.key not in ghosts_keys]
        if len(ghosts_to_choose) > nr_ghosts:
            ghosts = random.sample(ghosts_to_choose, nr_ghosts)
        else:
            ghosts = ghosts_to_choose

        print ghosts
        return ghosts

    def are_ghosts_available(self):
        """
        Method that returns whether there are any Ghost instances that are not
        reserved
        """
        # check if there are ghosts available
        ghosts_available = False
        ghosts_list = Ghost.query(ancestor=ghost_master_key()).fetch()
        for ghost in ghosts_list:
            if not ghost.is_reserved:
                ghosts_available = True
                break
        return ghosts_available

    @decorator.oauth_required
    def get(self):
        """
        Renders a form that contains max three random choices for a user's
        Ghost Name
        """
        userdata = self.get_user_data()

        phantom = Phantom.query(ancestor=phantom_name_master_key()). \
            filter(Phantom.user.user_id == userdata.user_id).get()

        if not phantom:
            self.redirect('/add-phantom/')

        ghosts_available = self.are_ghosts_available()
        print ghosts_available
        if not ghosts_available:
            # TODO: ADD Session SessionDict to display flash message if
            # redirecting
            # to overview page saying that sorry no more ghost names left
            return self.redirect('/overview/')

        # create form and pass choices

        return self.render('manage_phantom_name_second.html', {
            'userdata': userdata,
            'user_is_admin': users.is_current_user_admin(),
            'ghosts_to_choose': self.get_random_free_ghosts(3),
            'phantom': phantom
        })

    @decorator.oauth_required
    def post(self):
        """
        Handles the POST of the chosen ghost by the user. In case the form
        is valid and the Ghost is not owned by someone else, the Ghost is saved
        to the Phantom instance, enabling the creation of the Phantom Ghost
        Name
        """
        ghost_id = self.request.get('ghost')
        userdata = self.get_user_data()
        errors = []

        if ghost_id:
            ghost_id = int(ghost_id)
            ghost = Ghost.get_by_id(ghost_id, parent=ghost_master_key())

            # check if the ghost is owned by someone else
            if not ghost.is_reserved:
                phantom = Phantom.query(
                    ancestor=phantom_name_master_key()).filter(
                        Phantom.user.user_id == userdata.user_id).get()
                phantom.ghost = ghost.key
                phantom.put()
                return self.redirect('/overview/')
            else:
                # check if there are ghosts available
                ghosts_available = self.are_ghosts_available()

                if not ghosts_available:
                    # TODO: ADD Session SessionDict to display flash message if
                    # redirecting
                    # to overview page saying that sorry no more ghost names
                    # left
                    return self.redirect('/overview/')
                errors.append('Ghost has been reserved by someone else in the '
                              'meantime. Please choose another one')
        else:
            # in case no id
            errors.append('An option is required')

        phantom = Phantom.query(ancestor=phantom_name_master_key()). \
            filter(Phantom.user.user_id == userdata.user_id).get()

        # send back for the user to choose
        return self.render('manage_phantom_name_second.html', {
            'userdata': userdata,
            'user_is_admin': users.is_current_user_admin(),
            'ghosts_to_choose': self.get_random_free_ghosts(3),
            'phantom': phantom,
            'errors': errors
        })

    def DenyAccess(self):
        """Redirects the user to log in in case access is denied"""
        self.redirect(users.create_login_url(self.request.uri))

    def XsrfFail(self):
        """Renders an errors page in case Xsrf failed"""
        self.render('errors.html', {'error': 'XSRF FAILED'})


class ManageGhost(handlers.AuthenticatedHandler, UserInfoMixin):
    """Handler rendering a page that enables users to edit or add Ghost
    Names"""

    @decorator.oauth_required
    @whitelisted_required
    def get(self, ghost_id=None):
        """Get request handler that renders a Ghost form empty or
        pre-populated for editing"""

        if ghost_id:
            ghost = Ghost.get_by_id(int(ghost_id), parent=ghost_master_key())
            form = GhostForm(obj=ghost)
        else:
            form = GhostForm()

        return self.render('manage_ghost.html', {'form': form})

    @decorator.oauth_required
    @whitelisted_required
    def post(self, ghost_id=None):
        """Post request handler that handles editing or adding new Ghost
        names"""
        userdata = self.get_user_data()
        form = GhostForm(self.request.POST)
        if form.validate():
            if form.id.data:
                ghost_to_edit = Ghost.get_by_id(int(ghost_id),
                                                parent=ghost_master_key())
                form.populate_obj(ghost_to_edit)
                ghost_to_edit.put()
            else:
                new_ghost = Ghost(parent=ghost_master_key(), creator=userdata)
                form.populate_obj(new_ghost)
                new_ghost.put()

            self.redirect('/cms/ghosts/')
        else:
            # return form back and display errors
            return self.render('manage_ghost.html', {'form': form})

    def DenyAccess(self):
        """Redirects the user to log in in case access is denied"""
        self.redirect(users.create_login_url(self.request.uri))

    def XsrfFail(self):
        """Renders an errors page in case Xsrf failed"""
        self.render('errors.html', {'error': 'XSRF FAILED'})


class DeleteGhost(handlers.AuthenticatedHandler):
    """Handler that manages the deletion of a Ghost Name"""

    @decorator.oauth_required
    @whitelisted_required
    def post(self):
        """Post request method handling the deletion of a Ghost name"""
        ghost_id = int(self.request.get('ghost_id'))
        if ghost_id:
            ghost_instance = Ghost.get_by_id(ghost_id,
                                             parent=ghost_master_key())
            if ghost_instance:
                ghost_instance.key.delete()
        self.redirect('/cms/ghosts/')

    def DenyAccess(self):
        """Redirects the user to log in in case access is denied"""
        self.redirect(users.create_login_url(self.request.uri))

    def XsrfFail(self):
        """Renders an errors page in case Xsrf failed"""
        self.render('errors.html', {'error': 'XSRF FAILED'})


class WhitelistedEmails(handlers.AuthenticatedHandler, UserInfoMixin):
    """Handler that manages the display and addition of Whitelisted emails"""

    @decorator.oauth_required
    @whitelisted_required
    def get(self):
        """Get request handler that renders a list of whitelisted emails as
        well as a form that enables the additional of new whitelisted emails"""
        form = WhiteListForm()
        whitelist = Whitelist.query(ancestor=whitelist_master_key()).fetch()
        userdata = self.get_user_data()
        return self.render('whitelist_cms.html',
                           {'whitelist': whitelist,
                            'form': form,
                            'userdata': userdata})

    @decorator.oauth_required
    @whitelisted_required
    def post(self):
        """Post request handler managing the addition of new Whitelisted
        emails"""
        form = WhiteListForm(self.request.POST)
        userdata = self.get_user_data()
        if form.validate():
            whitelist_instance = Whitelist(parent=whitelist_master_key())
            form.populate_obj(whitelist_instance)
            whitelist_instance.put()
            return self.redirect('/cms/whitelisted-emails/')
        else:
            whitelist = Whitelist.query(
                ancestor=whitelist_master_key()).fetch()
            return self.render('whitelist_cms.html', {
                'whitelist': whitelist,
                'form': form,
                'userdata': userdata
            })

    def DenyAccess(self):
        """Redirects the user to log in in case access is denied"""
        self.redirect(users.create_login_url(self.request.uri))

    def XsrfFail(self):
        """Renders an errors page in case Xsrf failed"""
        self.render('errors.html', {'error': 'XSRF FAILED'})


class DeleteEmailWhitelist(handlers.AuthenticatedHandler):
    """Handler that manages the deletion of Whitelisted Emails"""

    @decorator.oauth_required
    @whitelisted_required
    def post(self):
        """Post request method that deletes a specific whitelisted email"""
        whitelist_obj_id = int(self.request.get('superhero_id'))
        if whitelist_obj_id:
            whitelist_instance = Whitelist.get_by_id(
                whitelist_obj_id, parent=whitelist_master_key())
            if whitelist_instance:
                whitelist_instance.key.delete()
        self.redirect('/cms/whitelisted-emails/')

    def DenyAccess(self):
        """Redirects the user to log in in case access is denied"""
        self.redirect(users.create_login_url(self.request.uri))

    def XsrfFail(self):
        """Renders an errors page in case Xsrf failed"""
        self.render('errors.html', {'error': 'XSRF FAILED'})


class GhostCMSListView(handlers.AuthenticatedHandler):
    """Handler that displays a list of Ghost Names where
    admins/whitelisted users can manage them"""

    @decorator.oauth_required
    @whitelisted_required
    def get(self):
        """Get request handler returning a list of Ghost Names"""
        ghosts = Ghost.query(ancestor=ghost_master_key()).fetch()
        self.render('ghost_list_cms.html', {'ghosts': ghosts})

    def DenyAccess(self):
        """Redirects the user to log in in case access is denied"""
        self.redirect(users.create_login_url(self.request.uri))

    def XsrfFail(self):
        """Renders an errors page in case Xsrf failed"""
        self.render('errors.html', {'error': 'XSRF FAILED'})


class ImportGhosts(handlers.AuthenticatedHandler, UserInfoMixin):
    """Handler that manages the import of default Ghost Names"""

    @decorator.oauth_required
    @whitelisted_required
    def post(self):
        """Post request method that reads a csv file of Ghost Names and saves
        the ones that are not duplicate"""
        ghosts = []
        userdata = self.get_user_data()

        # Get all ghost names in the system
        ghost_names_existing = []
        ghost_query = Ghost.query(ancestor=ghost_master_key()).fetch()
        for ghost_item in ghost_query:
            print ghost_item.name.encode('utf8')
            ghost_names_existing.append(ghost_item.name)

        with open('ghost_names.csv', 'rb') as csvfile:
            reader = csv.reader(csvfile, delimiter=',', quotechar='|')
            for row in reader:
                if row[0] and unicode(row[0],
                                      'utf8') not in ghost_names_existing:
                    ghost_thing = {'name': unicode(row[0], 'utf8')}
                    if row[1]:
                        ghost_thing['description'] = unicode(row[1], 'utf8')
                    ghosts.append(ghost_thing)

        if ghosts:
            for ghost in ghosts:
                new_ghost = Ghost(parent=ghost_master_key(),
                                  name=ghost.get('name', ''),
                                  description=ghost.get('description', ''),
                                  creator=userdata)
                new_ghost.put()
        self.redirect('/cms/ghosts/')

    def DenyAccess(self):
        """Redirects the user to log in in case access is denied"""
        self.redirect(users.create_login_url(self.request.uri))

    def XsrfFail(self):
        """Renders an errors page in case Xsrf failed"""
        self.render('errors.html', {'error': 'XSRF FAILED'})
