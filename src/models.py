from google.appengine.ext import ndb
from master_keys import phantom_name_master_key
from master_keys import ghost_master_key
from master_keys import whitelist_master_key


class UserData(ndb.Model):
    """Model that defines fields for identify a user"""
    user_id = ndb.StringProperty()
    email = ndb.StringProperty()

    @property
    def is_whitelisted(self):
        """Method that retuns whether the userdata is whitelisted"""
        return bool(Whitelist.query(ancestor=whitelist_master_key()).filter(
            Whitelist.email == self.email).get())

    @property
    def has_phantom_name(self):
        """Method that returns whether the userdata has a phantom name
        associated with it"""
        phantom = Phantom.query(ancestor=phantom_name_master_key()).filter(
            Phantom.user.user_id == self.user_id).get()
        if phantom:
            return bool(phantom.display_name)
        return False


class Ghost(ndb.Model):
    """Model that defines fields for identify a Ghost"""
    name = ndb.StringProperty(required=True)
    name_lower = ndb.ComputedProperty(lambda self: self.name.lower())
    description = ndb.TextProperty()
    creator = ndb.StructuredProperty(UserData)

    @property
    def is_reserved(self):
        """Checks whether a Phantom links to this Ghost or not and returns
        a bool"""
        return bool(Phantom.query(ancestor=phantom_name_master_key()).
                    filter(Phantom.ghost == self.key).get())


class Phantom(ndb.Model):
    """Model that defines fields for a Phantom"""
    user = ndb.StructuredProperty(UserData)
    first_name = ndb.StringProperty(required=True)
    last_name = ndb.StringProperty(required=True)
    ghost = ndb.KeyProperty(kind=Ghost)

    @property
    def display_name(self):
        """This returns the display name of a Phantom Ghost"""
        ghost_name = None
        if self.ghost:
            ghost_name = getattr(
                Ghost.query(ancestor=ghost_master_key()).filter(
                    Ghost.key == self.ghost).get(), 'name', None)
        if self.first_name and ghost_name and self.last_name:
            return self.first_name + " '" + ghost_name + "' " + self.last_name
        return ''


class Whitelist(ndb.Model):
    """Model that defines whitelisted emails"""
    email = ndb.StringProperty(required=True)
