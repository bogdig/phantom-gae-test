/**
 * @fileoverview Entry point for GAE Scaffold application.
 */

goog.provide('app');
goog.require('goog.events');
goog.require('goog.array');
goog.require('goog.dom');
goog.require('goog.dom.dataset');
goog.require('goog.ui.Tooltip');

/**
 * Entry point for GAE scaffold application.
 */
app.main = function() {
  console.log('app.main() entry point');
  var ghosts = goog.dom.getElementsByClass('ghost-overview-item');

  goog.array.forEach(ghosts, function(ghost_elem){
    var node = goog.dom.getFirstElementChild(ghost_elem);
    var ghost_description = goog.dom.dataset.get(node, 'description');
    var tooltip = new goog.ui.Tooltip(node, ghost_description);
    tooltip.className = 'ghost-tooltip';
  });


}

goog.exportSymbol('app.main', app.main);

goog.events.listen(window, goog.events.EventType.LOAD, app.main);
